use std::fmt::Display;

pub enum Arg<S>
where
    S: Display,
{
    Positional(S),
    Flag(S),
    FlagEquals(S, S),
}

impl<S> std::fmt::Display for Arg<S>
where
    S: Display,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Arg::Positional(text) => write!(f, "Positional: '{}'", text),
            Arg::Flag(text) => write!(f, "Flag: -{}", text),
            Arg::FlagEquals(key, value) => write!(f, "Flag with argument: -{} = {}", key, value),
        }
    }
}

pub fn parse_args(args: &[String]) -> Vec<Arg<String>> {
    let mut parsed: Vec<Arg<String>> = vec![];

    for arg in args {
        let c_arg: Vec<char> = arg.chars().collect();
        if c_arg.get(0).unwrap() == &'-' {
            let mut text_start = 1;
            if let Some(&'-') = c_arg.get(text_start) {
                text_start = 2;
            }
            // flag
            if let Some(eq_pos) = arg.find('=') {
                //flag_equals
                let key = &arg[text_start..eq_pos];
                let value = &arg[eq_pos + 1..];
                parsed.push(Arg::FlagEquals(key.to_owned(), value.to_owned()));
            } else {
                if text_start == 1 {
                    // collection of short flags
                    for c in &c_arg[text_start..] {
                        parsed.push(Arg::Flag(c.to_string()));
                    }
                } else {
                    // one long flag
                    let text = &arg[text_start..];
                    parsed.push(Arg::Flag(text.to_owned()));
                }
            }
        } else {
            // positional
            parsed.push(Arg::Positional(
                // have to slice here to get around arg being a &&str
                arg[..].to_owned(),
            ));
        }
    }

    parsed
}
